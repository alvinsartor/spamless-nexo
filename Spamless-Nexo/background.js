'use strict';

const code = `
// callback executed when canvas was found
function cleanPageFromBanners() {
  document.getElementsByClassName('NexoBenefits')[0].remove();
  document.getElementsByClassName('banners')[0].remove();
  document.getElementsByClassName('actions')[0].remove();

  const financialOverview = document.getElementsByClassName('AccountFinancialOverview')[0];
  financialOverview.querySelector('.right').remove()
  financialOverview.querySelector('.center').remove()
  financialOverview.querySelector('.left').setAttribute('class', 'center');
}

// set up the mutation observer
var observer = new MutationObserver(function (mutations, me) {
  var banners = document.getElementsByClassName('NexoBenefits')[0];
  if (banners) {
    cleanPageFromBanners();   
    return;
  }
});

// start observing
observer.observe(document, {
  childList: true,
  subtree: true
});
`

const executeScript = (details) => chrome.tabs.executeScript(details.tabId, { code: code });
chrome.webNavigation.onCompleted.addListener(executeScript, { url: [{ hostContains: 'platform.nexo.io' }]});